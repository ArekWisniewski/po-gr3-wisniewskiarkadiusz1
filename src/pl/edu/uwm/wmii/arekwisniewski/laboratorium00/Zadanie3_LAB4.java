package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zadanie3_LAB4 {
    static int count_substring(String path, String subStr) throws Exception{
        File given_file = new File(path);
        Scanner myReader = new Scanner(given_file);
        int counter = 0;
        int word_num = 0;
        while (myReader.hasNextLine()) {
            String data = myReader.nextLine();
            for (int i = 0; i < data.length(); i++){
                if (data.charAt(i) == subStr.charAt(counter)){
                    counter++;
                    if (counter == subStr.length()){
                        word_num++;
                        counter = 0;
                    }
                } else {
                    counter = 0;
                }
            }
        }
        return word_num;
    }

    public static void main(String[] args) {
        int word_num = 0;
        String path = "C:\\Users\\lol9a\\IdeaProjects\\po-gr3-wisniewskiarkadiusz1\\src\\pl\\edu\\uwm\\wmii\\arekwisniewski\\laboratorium00\\test.txt";
        try {
            word_num = count_substring(path, "program");
        } catch (Exception b){
            System.out.println("Brak pliku w podanej sciezce");
        }

        System.out.println("Liczba wybranych słów: "+word_num);
    }
}
