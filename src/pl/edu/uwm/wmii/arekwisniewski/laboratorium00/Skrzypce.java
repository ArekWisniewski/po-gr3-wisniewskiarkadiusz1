package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

import java.time.LocalDate;

public class Skrzypce extends Instrument{
    public Skrzypce(String prod, LocalDate rP){
        super(prod, rP);
    }
    public String dzwiek(){
        return "skrzyp";
    }
}
