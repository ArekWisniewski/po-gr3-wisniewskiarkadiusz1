package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

public class Zadanie1g_LAB4 {
    public static void main(String[] args) {

        System.out.println(nice("1234567890"));
    }

    public static StringBuilder nice(String str) {
        StringBuilder s = new StringBuilder(str);
        int id = (str.length() - 3);

        while (id > 0)
        {
            s.insert(id, ",");
            id = id - 3;
        }
        return s;
    }
}
