package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

public class Zad1_2 {
    public static void main(String[] args) {

        int [] arr = new int [] {1, 2, 3, 4, 5};
        int n = 4;

        System.out.println("Original: ");
        for (int k : arr) {
            System.out.print(k + " ");
        }
        for(int i = 0; i < n; i++){
            int j, last;
            last = arr[arr.length-1];

            for(j = arr.length-1; j > 0; j--){
                arr[j] = arr[j-1];
            }
            arr[0] = last;
        }
        System.out.println();
        System.out.println("After right rotation: ");
        for (int j : arr) {
            System.out.print(j + " ");
        }
    }
}

