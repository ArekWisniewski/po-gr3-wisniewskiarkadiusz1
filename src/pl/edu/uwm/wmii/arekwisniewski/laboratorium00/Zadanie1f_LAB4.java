package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

public class Zadanie1f_LAB4 {
    static String change(String str){
        StringBuffer s = new StringBuffer();
        for(int i = 0; i < str.length(); i++){
            if(str.charAt(i)>='a' && str.charAt(i)<='z'){
                s.append((char)(str.charAt(i) - 32));
            }else if((str.charAt(i)>='A' && str.charAt(i)<='Z')){
                s.append((char)(str.charAt(i) + 32));
            }else s.append(str.charAt(i));
        }
        return s.toString();
    }

    public static void main(String[] args) {
        String ex = "ABCfg?nGwq%oSe!jfq*Da";
        System.out.println(change(ex));


    }
}
