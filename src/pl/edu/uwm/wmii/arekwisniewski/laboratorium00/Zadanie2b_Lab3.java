package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

import java.util.Random;

public class Zadanie2b_Lab3 {
    public static void generuj (int[] tab, int n, int minWartosc, int maxWartosc){
        Random random = new Random();
        for(int i=0; i<n; i++) {
            tab[i] = random.nextInt((maxWartosc-minWartosc)+1) + minWartosc;
        }
    }
    public static void wypisz(int[] tab, int n){
        for(int i=0; i<n; i++) {
            System.out.println(tab[i]);
        }
    }

    public static int ileDodatnich (int[] tab){
        int positive = 0;
        for(int j : tab) {
            if (j > 0) positive += 1;
        }
        return positive;
    }

    public static int ileUjemnych (int[] tab){
        int negative = 0;
        for(int j : tab) {
            if (j < 0) negative += 1;
        }
        return negative;
    }

    public static int ileZerowych (int[] tab){
        int zeros = 0;
        for(int j : tab) {
            if (j == 0) zeros += 1;
        }
        return zeros;
    }

    public static void main(String[] args) {

        // creating n
        Random random = new Random();
        int n;
        int max_n = 100;
        int min_n = 1;
        n = random.nextInt((max_n - min_n) + 1) + min_n;

        int[] array = new int[n];
        int max = 999;
        int min = -999;
        generuj(array,n,min,max);
        wypisz(array,n);
        System.out.format("number of positive: %d%n",ileDodatnich(array));
        System.out.format("number of negative: %d%n",ileUjemnych(array));
        System.out.format("number of zeros: %d%n",ileZerowych(array));

    }
}
