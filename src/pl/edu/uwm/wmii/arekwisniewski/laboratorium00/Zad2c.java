package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

public class Zad2c {

    public static void main (String[] args){

        double[] sequence = {1,2,4,6,9,16,36,64,100};
        int count = 0;
        for (double v : sequence) {
            if (v % 4 == 0 & Math.pow(v, 2) % 2 == 0) {
                count += 1;
                System.out.println("The number " + v + " is a square of an even number");
            }
        }

        System.out.println(count);
    }
}