package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

public class IntegerSET {
    private final boolean[] zbior;
    public IntegerSET(){
        this.zbior=new boolean[100];
    }
    public static IntegerSET union(IntegerSET a, IntegerSET b){
        IntegerSET n_zbior=new IntegerSET();
        for(int i=0;i<100;i++){
            if(a.zbior[i] || b.zbior[i])n_zbior.zbior[i]=true;
        }
        return n_zbior;
    }
    public static IntegerSET intersection(IntegerSET a, IntegerSET b){
        IntegerSET n_zbior= new IntegerSET();
        for(int i=0;i<100;i++){
            if(a.zbior[i] && b.zbior[i])n_zbior.zbior[i]=true;
        }
        return n_zbior;
    }
    public void insertElement(int i){
        this.zbior[i-1]=true;
    }
    public void deleteElement(int i){
        this.zbior[i-1]=false;
    }
    @Override
    public String toString(){
        StringBuilder liczby= new StringBuilder();
        for(int i=0;i<100;i++){
            if(this.zbior[i]){
                liczby.append(i+1);
                liczby.append(" ");
            }
        }
        return liczby.toString();
    }
    public boolean equals(IntegerSET b){
        return this.toString().equals(b.toString());
    }
}