package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;
import java.util.ArrayList;

public class Zadannie5 {
    public static void reverse(ArrayList<Integer> L) {
        int temp;
        for(int i = L.size()-1; i >= 0; i--)
            L.add(L.get(i));
        for(int j = 0; j < L.size(); j++)
            L.remove(L.get(j));
    }
    public static void main(String[] args) {
        ArrayList<Integer> arrL = new ArrayList<Integer>(4);
        arrL.add(4);
        arrL.add(3);
        arrL.add(2);
        arrL.add(1);
        System.out.println("Przed odwroceniem: "+arrL);
        reverse(arrL);
        System.out.print("Po odwroceniu: "+arrL);


    }
}

