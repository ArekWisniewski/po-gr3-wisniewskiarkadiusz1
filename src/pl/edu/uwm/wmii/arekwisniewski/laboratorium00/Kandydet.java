package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

import java.util.ArrayList;
import java.util.HashMap;


class Kandydet implements Cloneable, Comparable<Kandydet> {
    private String nazwa;
    private int wiek;
    private String wyksztalcenie;
    private int doswiadczenie;

    public Kandydet(String nazwa, int wiek, String wyksztalcenie, int doswiadczenie) {
        this.nazwa = nazwa;
        this.wiek = wiek;
        this.wyksztalcenie = wyksztalcenie;
        this.doswiadczenie = doswiadczenie;
    }

    @Override
    public int compareTo(Kandydet other) {

        if (this.wyksztalcenie.compareTo(other.wyksztalcenie) != 0) {
            return this.wyksztalcenie.compareTo(other.wyksztalcenie);
        }

        if (this.wiek != other.wiek) {

            if (this.wiek < other.wiek)
                return -1;
            else
                return 1;

        }

        if (this.doswiadczenie < other.doswiadczenie)
            return -1;

        if (this.doswiadczenie > other.doswiadczenie)
            return 1;

        return 0;
    }

    public String getNazwa() {

        return nazwa;
    }

    public int getDoswiadczenie() {

        return doswiadczenie;
    }

    public int getWiek() {

        return wiek;
    }

    public String getWyksztalcenie() {

        return wyksztalcenie;
    }

    public static boolean Qualified(Kandydet k) {
        if(k.getWyksztalcenie() >= Rekrutacja.doswiadczenie) {
            return true;
        }
        return false;
    }

    public static HashMap<Integer, String> RecruitmentMap(ArrayList<Kandydet> klist) {
        HashMap<Integer, String>  map = new HashMap<>();
        for(Kandydet e : klist)
            if(Qualified(e))
                map.put(e.getDoswiadczenie(), e.getNazwa());
        return map;
    }

}