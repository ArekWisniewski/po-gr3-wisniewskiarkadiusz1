package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

import java.time.LocalDate;

public class Osoba2 implements Cloneable, Comparable<Osoba2>{
    public Osoba2(String naz, LocalDate dU){
        this.dataUrodzenia=dU;
        this.nazwisko=naz;
    }

    public String getNazwisko() {
        return this.nazwisko;
    }

    public LocalDate getDataUrodzenia() {
        return this.dataUrodzenia;
    }

    @Override
    public boolean equals(Object obj2) {
        Osoba2 osb2 = (Osoba2) obj2;
        return (osb2.nazwisko.equals(this.nazwisko) && osb2.dataUrodzenia.equals(this.dataUrodzenia));
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()+"["+this.nazwisko+", "+this.dataUrodzenia.toString()+"]";
    }

    @Override
    public int compareTo(Osoba2 o2) {
        int compare_nazwisko= this.nazwisko.compareTo(o2.nazwisko);
        if(compare_nazwisko==0){
            return this.dataUrodzenia.compareTo(o2.dataUrodzenia);
        }
        return compare_nazwisko;
    }

    private String nazwisko;
    private LocalDate dataUrodzenia;
}
