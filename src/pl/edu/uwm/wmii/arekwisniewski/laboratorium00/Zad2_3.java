package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

public class Zad2_3 {
    public static void main(String[] args) {
        int[] sequence = {6, 0, 1, -5, -12, -1, -3, 0, 3, 8};
        int pos = 0;
        int neg = 0;
        int zero = 0;

        for (int j : sequence) {
            if (j == 0)
                zero++;
            if (j > 0)
                pos++;
            if (j < 0)
                neg++;
        }
        System.out.println("" + pos + " positive numbers.");
        System.out.println("" + neg + " negative numbers.");
        System.out.println("" + zero + " zeroes.");
    }
}
