package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

public class Zad2_4 {
    public static void main(String[] args){

        int[] sequence = {3,15,25,800,-180,8,9,11};
        int smallest = 999;
        int largest = 0;

        for(int i = sequence.length - 1; i >= 0; i--){
            if(sequence[i] < smallest)
                smallest = sequence[i];
            if(sequence[i] > largest)
                largest = sequence[i];
        }
        System.out.println("Smallest number: " + smallest);
        System.out.println("Largest number: " + largest);
    }

}