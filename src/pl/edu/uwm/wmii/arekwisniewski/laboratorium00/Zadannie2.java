package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;
import java.util.ArrayList;

public class Zadannie2 {

    public static ArrayList<Integer> merge(ArrayList<Integer> arrL1, ArrayList<Integer> arrL2) {
        ArrayList<Integer> new_arrL = new ArrayList<Integer>(arrL1.size() + arrL2.size());
        if (arrL1.size() == arrL2.size()) {
            for (int i = 0, j = 0; i < arrL1.size(); i++, j += 2) {
                new_arrL.add(j, arrL1.get(i));
                new_arrL.add(j + 1, arrL2.get(i));
            }

        } else {
            int shorter = Math.min(arrL1.size(), arrL2.size());
            int longer = Math.max(arrL1.size(), arrL2.size());
            if (arrL1.size() < arrL2.size()) {
                for (int i = 0, j = 0; i < shorter; i++, j += 2) {
                    new_arrL.add(j, arrL1.get(i));
                    new_arrL.add(j + 1, arrL2.get(i));
                }
                int x = 0;
                while(new_arrL.size()<longer+shorter){
                    new_arrL.add(2*shorter + x, arrL2.get(shorter+x));
                    x++;
                }

            } else {
                for (int i = 0, j = 0; i < shorter; i++, j += 2) {
                    new_arrL.add(j, arrL2.get(i));
                    new_arrL.add(j + 1, arrL1.get(i));
                }
                int x = 0;
                while(new_arrL.size()<longer+shorter){
                    new_arrL.add(2*shorter + x, arrL1.get(shorter+x));
                    x++;
                }
            }
        }
        return new_arrL;
    }

    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<Integer>(4);
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);

        ArrayList<Integer> b = new ArrayList<Integer>(5);
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);

        ArrayList<Integer> c = new ArrayList<Integer>(5);
        c.add(1);
        c.add(3);
        c.add(6);
        c.add(1);
        c.add(9);

        System.out.println("\n" + b.toString() + "\n" + c.toString());
        System.out.println("Pierwszy rowny rozmiarowi drugiego: " +merge(b,c));

        System.out.println("\n" + a.toString() + "\n" + b.toString());
        System.out.println("Pierwszy jest mniejszy niz drugi: " +merge(a,b));

        System.out.println("\n" + b.toString() + "\n" + a.toString());
        System.out.println("Pierwszy jest wiekszy niz drugi: " +merge(b,a));

    }
}