package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

import java.time.LocalDate;

public class Fortepian extends Instrument{
    public Fortepian(String prod, LocalDate rP){
        super(prod, rP);
    }
    public String dzwiek(){
        return "pam pam";
    }
}
