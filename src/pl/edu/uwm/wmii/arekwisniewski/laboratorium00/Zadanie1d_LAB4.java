package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

public class Zadanie1d_LAB4 {
    public static String repeat(String str, int n){
        String new_string = "";
        for(int i=0; i<n; i++){
            new_string += str;
        }
        return new_string;
    }
    public static void main(String[] args) {
        String str = "ha";
        int n = 10;
        System.out.println("Wyrazenie '"+str+"' powtorzone "+n+" razy to: "+repeat(str, n));

    }
}

