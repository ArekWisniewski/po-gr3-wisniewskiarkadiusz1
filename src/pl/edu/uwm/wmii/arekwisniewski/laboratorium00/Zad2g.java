package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

public class Zad2g {
    public static void main(String[] args) {

        int[] sequence = new int[]{1, 5, 9, -2, 4, 12, -1, -12};

        int count = 0;
        for(int i =0; i< sequence.length; i++) {
            if(i % 2 != 0 & sequence[i] > 0) {
                count++;
                System.out.println("The number " + sequence[i] + " satisfies all criteria.");
            }
        }
        System.out.println(count);
    }
}
