package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import static pl.edu.uwm.wmii.arekwisniewski.laboratorium00.Kandydet.Qualified;
import static pl.edu.uwm.wmii.arekwisniewski.laboratorium00.Kandydet.RecruitmentMap;

public class Main {

    public static void main(String[] args) {
        ArrayList<Kandydet> grupa = new ArrayList<>();
        grupa.add(new Kandydet("Mariusz Pudzianowski", 43, "inzynier", 1));
        grupa.add(new Kandydet("Jerzy Dudek", 40, "inzynier", 2));
        grupa.add(new Kandydet("Jerzu urban", 30, "doktor", 5));
        grupa.add(new Kandydet("Mateusz Siekierski", 29, "doktor", 6));
        grupa.add(new Kandydet("Olaf Lubaszenko", 50,"magister",3));


        Rekrutacja.setDoswiadczenie();

        System.out.print("\n Kto sie zakwalifikowany:\n");
        for (Kandydet e : grupa) {
            System.out.println(e.getNazwa() + " " + e.getDoswiadczenie() + " : " + Qualified(e));
        }

        System.out.print("\n Mapa Zakwalifikowanych:\n");
        HashMap<Integer, String> map = RecruitmentMap(grupa);
        for (Integer e : map.keySet()) {
            System.out.println(e + " " + map.get(e));
        }

    }


}