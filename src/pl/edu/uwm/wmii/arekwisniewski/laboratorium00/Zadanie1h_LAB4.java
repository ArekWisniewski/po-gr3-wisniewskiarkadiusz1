package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

public class Zadanie1h_LAB4 {

    public static void main(String[] args) {

        System.out.println(nice("1234567890", "!", 0));
    }

    public static StringBuilder nice(String str, String select, int sep) {
        StringBuilder s = new StringBuilder(str);
        int id = (str.length() - sep);

        if (sep < 1)
            return s;

        while (id > 0)
        {
            s.insert(id, select);
            id = id - sep;
        }
        return s;
    }
}
