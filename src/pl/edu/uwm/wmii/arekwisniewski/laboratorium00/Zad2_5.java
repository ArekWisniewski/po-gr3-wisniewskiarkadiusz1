package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

public class Zad2_5 {
    public static void main(String[] args){
        int[] sequence = {3,15,25,800,-180,8,9,11};
        int pairs = 0;
        for(int i = 0; i< sequence.length-1; i++) {

            if (sequence[i] > 0 & sequence[i + 1] > 0) {
                pairs++;
                System.out.println("The positive pairs (" + sequence[i] + "," + sequence[i + 1] + ") exist.");
            }
        }
        System.out.println("Number of pairs: " +pairs);
    }
}
