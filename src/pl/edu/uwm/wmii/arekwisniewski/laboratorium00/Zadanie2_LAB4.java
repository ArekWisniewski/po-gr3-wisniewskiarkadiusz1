package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zadanie2_LAB4 {
    static int count_chars(String path, char character) throws Exception{
        File given_file = new File(path);
        Scanner myReader = new Scanner(given_file);
        int counter = 0;
        while (myReader.hasNextLine()) {
            String data = myReader.nextLine();
            for (int i = 0; i < data.length(); i++){
                if (data.charAt(i) == character){
                    counter++;
                }
            }
        }
        return counter;
    }

    public static void main(String[] args) {
        int char_num = 0;
        String path = "C:\\Users\\Arek_\\IdeaProjects\\po-gr3-wisniewskiarkadiusz1\\src\\pl\\edu\\uwm\\wmii\\arekwisniewski\\laboratorium00\\test.txt";
        try {
            char_num = count_chars(path, 'N');
        } catch (Exception a){
            System.out.println("Brak pliku w podanej sciezce");
        }

        System.out.println("Liczba wybranych znakow: "+char_num);
    }
}
