package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

import java.time.LocalDate;

public abstract class Osoba1 {
    public Osoba1(String nazwisko, String[] imi, LocalDate DU, boolean gender)
    {
        this.nazwisko = nazwisko;
        this.imiona=imi;
        this.DataUrodzenia=DU;
        this.plec=gender;
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }

    public String[] getImiona() {
        return imiona;
    }

    public LocalDate getDataUrodzenia() {
        return DataUrodzenia;
    }

    public boolean isPlec() {
        return plec;
    }

    private String nazwisko;
    private String[] imiona;
    private LocalDate DataUrodzenia;
    private boolean plec;
}