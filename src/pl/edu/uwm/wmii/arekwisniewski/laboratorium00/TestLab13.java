package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

import java.io.FileNotFoundException;

public class TestLab13 {
    public static void main(String[] args) throws FileNotFoundException {

        Egzamin po = new Egzamin();
        po.dodaj("Kowal", "db");
        po.dodaj("Brodowski","dbd");
        po.dodaj("Wieczorek","dst");
        po.dodaj("Smolka","db+");
        po.wypisz();
        po.usun("Wieczorek");
        po.zmien("Smolka","dst+");
        po.wypisz();

        Egzamin2 po2= new Egzamin2();
        po2.dodaj(new StudentLab13("Sebastian","Kowal",4),"bdb");
        po2.dodaj(new StudentLab13("Adam","Kowal",3),"db");
        po2.dodaj(new StudentLab13("Arek","Brodowski",1),"dst");
        po2.dodaj(new StudentLab13("Konrad","Radziejewski",2),"ndst");
        po2.wypisz();
        po2.usun(3);
        po2.zmien(2,"dst+");
        po2.wypisz();

    }
}
