package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

import java.time.LocalDate;

public class Student2 extends Osoba2 implements Cloneable, Comparable<Osoba2>{
    public Student2(String naz, LocalDate dU, double sO) {
        super(naz, dU);
        this.sredniaOcen=sO;
    }

    private double sredniaOcen;

    @Override
    public String toString() {
        return this.getClass().getSimpleName()+"["+this.getNazwisko()+", "+this.getDataUrodzenia().toString()+", "+this.sredniaOcen+"]";
    }

    @Override
    public int compareTo(Osoba2 o2) {
        int last=super.compareTo((o2));
        if((o2 instanceof Student2)&&(last==0)){
            return -(int)Math.ceil(this.sredniaOcen-((Student2) o2).sredniaOcen);
        }
        return last;
    }
}
