package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;


public class TestJava10 {
    public static void main(String[] args) {
        //TEST Zadanie1

        ArrayList<Osoba2> listaOsob = new ArrayList<>(5);
        listaOsob.add(new Osoba2("Giermyk", LocalDate.of(1969, 12, 2)));
        listaOsob.add(new Osoba2("Kamilek", LocalDate.of(1969, 12, 2)));
        listaOsob.add(new Osoba2("Pok", LocalDate.of(1999, 2, 8)));
        listaOsob.add(new Osoba2("Stoch", LocalDate.of(1999, 2, 8)));
        listaOsob.add(new Osoba2("Adas", LocalDate.of(1999, 12, 1)));
        System.out.println(listaOsob.get(0));
        System.out.println(listaOsob.get(0).equals(listaOsob.get(1)));
        System.out.println(listaOsob.get(0).equals(listaOsob.get(4)));

        System.out.println(listaOsob);
        Collections.sort(listaOsob);
        System.out.println(listaOsob);

        //Zadanie2

        ArrayList<Osoba2> listaStudentow = new ArrayList<>(5);
        listaStudentow.add(new Student2("Giermyk", LocalDate.of(1969, 12, 2), 3.09));
        listaStudentow.add(new Student2("Kamilek", LocalDate.of(1969, 12, 2), 4.76));
        listaStudentow.add(new Student2("Pok", LocalDate.of(1999, 2, 8), 4.0));
        listaStudentow.add(new Student2("Stoch", LocalDate.of(1999, 2, 8), 5.0));
        listaStudentow.add(new Student2("Adas", LocalDate.of(1999, 12, 1), 3.89));

        System.out.println(listaStudentow);
        Collections.sort(listaStudentow);
        System.out.println(listaStudentow);

    }
}