package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

public class Zad2_2 {
    public static void main(String[] args) {
        int[] sequence = {1, 5, 12, -4, 6, -1, -3, 10, 9, 8};
        int total = 0;

        for (int j : sequence) {
            if (j > 0) {
                total = total + j;
            }
        }
        System.out.println("Before doubling: " + total);
        System.out.println("After doubling: " + (total*2));
    }
}
