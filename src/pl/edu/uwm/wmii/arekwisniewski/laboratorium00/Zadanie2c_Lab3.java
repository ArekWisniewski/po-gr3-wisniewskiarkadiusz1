package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

import java.util.Random;

public class Zadanie2c_Lab3 {
    public static void generuj (int[] tab, int n, int minWartosc, int maxWartosc){
        Random random = new Random();
        for(int i=0; i<n; i++) {
            tab[i] = random.nextInt((maxWartosc-minWartosc)+1) + minWartosc;
        }
    }
    public static void wypisz(int[] tab, int n){
        for(int i=0; i<n; i++) {
            System.out.println(tab[i]);
        }
    }
    public static int ileMaksymalnych (int[] tab) {
        int max_value = -999;
        int max_value_instances = 0;
        for (int i : tab) {
            if (i > max_value) {
                max_value = i;
                max_value_instances += 1;

            }
        }
        System.out.print("("+max_value+")" );
        return max_value_instances;

    }
    public static void main(String[] args) {

        // creating n
        Random random = new Random();
        int n;
        int max_n = 100;
        int min_n = 1;
        n = random.nextInt((max_n - min_n) + 1) + min_n;

        int[] array = new int[n];
        int max = 999;
        int min = -999;
        generuj(array,n,min,max);
        wypisz(array,n);
        System.out.println("");
        System.out.println("number of max value instances: "+ileMaksymalnych(array));

    }
}
