package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

public class Zadanie1c_LAB4 {
    public static String middle(String str){
        String result;
        if(str.length()%2==0){
            double index = (str.length()-1)/2.0;
            int bound1 = (int) Math.floor(index);
            int bound2 = bound1 + 2;
            result = str.substring(bound1,bound2);
        }else{
            int bound1 = (str.length()-1)/2;
            int bound2 = bound1 + 1;
            result = str.substring(bound1,bound2);
        }
        return result;
    }
    public static void main(String[] args) {
        String str1 = "middle";
        String str2 = "honey";
        System.out.println("znak / znaki umieszczone pośrodku ciągu '"+str1+"' sa: "+middle(str1));
        System.out.println("znak / znaki umieszczone pośrodku ciągu '"+str2+"' sa: "+middle(str2));
    }
}
