package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

public class Zadanie1b_LAB4 {
    static int countSubStr(String str, String subStr) {
        int count = 0, fromIndex = 0;
        while ((fromIndex = str.indexOf(subStr, fromIndex)) != -1 ){
            System.out.println("Znaleziono w indexie: "+fromIndex);
            count++;
            fromIndex++;
        }
        return count;
    }
    public static void main(String[] args) {
        String str = "abbabaab";
        String subStr = "ab";
        System.out.println("Wyrazenie '"+subStr+"'w string '"+str+"' zostalo znalezione: "+countSubStr(str, subStr)+" razy.");
    }
}

