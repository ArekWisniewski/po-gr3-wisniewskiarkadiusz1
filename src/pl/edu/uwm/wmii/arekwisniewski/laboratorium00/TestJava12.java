package pl.edu.uwm.wmii.arekwisniewski.laboratorium00;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.LinkedList;

public class TestJava12 {
    public static void main(String[] args) {
        LinkedList<String> pracoownicy= new LinkedList<>();
        pracoownicy.add("Adam");
        pracoownicy.add("Bartek");
        pracoownicy.add("Lukasz");
        pracoownicy.add("Pawel");
        pracoownicy.add("Andrzej");
        pracoownicy.add("Stefan");
        pracoownicy.add("Edward");
        pracoownicy.add("Oskar");

        System.out.println(pracoownicy);
        Lab12.redukuj(pracoownicy,2);
        System.out.println(pracoownicy);
        LinkedList<Integer> liczby = new LinkedList<>();

        liczby.add(1);
        liczby.add(2);
        liczby.add(3);
        liczby.add(4);
        liczby.add(5);
        liczby.add(6);
        liczby.add(7);
        liczby.add(8);
        liczby.add(9);

        System.out.println(liczby);
        Lab12.redukuj(liczby,2);
        System.out.println(liczby);
        Lab12.odwroc(pracoownicy);
        System.out.println(pracoownicy);
        Lab12.odwroc(liczby);
        System.out.println(liczby);
        System.out.println(Lab12.slowaa("Ala ma kota. Jej kot lubi myszy."));
        Lab12.cyfry(2015);
        Lab12.Erastotenes(100);

        HashSet<LocalDate> daty = new HashSet<>();
        daty.add(LocalDate.of(2000,1,2));
        daty.add(LocalDate.of(1999,6,25));
        daty.add(LocalDate.of(1998,2,17));
        daty.add(LocalDate.of(1997,1,30));
        daty.add(LocalDate.of(1996,12,1));
        daty.add(LocalDate.of(1995,11,12));
        daty.add(LocalDate.of(1994,9,23));
        Lab12.print(liczby);
        Lab12.print(pracoownicy);
        Lab12.print(daty);
    }
}
